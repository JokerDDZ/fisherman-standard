﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FishingRod : MonoBehaviour
{
    public GameObject hook;
    public GameObject hookFloat;
    public GameObject progressBar;
    public GameObject rope;
    public GameObject littleFish;
    public GameObject Fish;
    public GameObject BigFish;
    public GameObject Rod;
    public GameObject player;
    public GameObject floatForFish;
    public GameObject fishingRodWithBones;

    public Hook hookMovement;
    public Color colorOfRod;
    public Shader s;
    public Renderer rodRenderer;
    public Rigidbody rb;
    public Text points;
    public Image progressBarPowerOfFish;

    private Animator am;

    public int whatFish = 0;
    public float power = 0f;
    public bool space = true;

    private float speedPower = 2000f;
    private const float maxPower = 2000f;
    private float timeAfterWeGetFish = 0f;
    private Vector3 offsetBeetweenHookAndFish = new Vector3(-0.59f, 1.593f, 0.17f);
    private float pointsNumber;
    private bool changeColor = false;
    private bool bend = false;
    private Vector3 myNormalRot;
    private Vector3 Left;

    private void Start()
    {
        am = GetComponent<Animator>();
        s = rodRenderer.material.shader;
    }

    private void Update()
    {
        if (space == true)
        {
            if (Input.GetMouseButton(0))
            {
                throwHook();             
            }

            if (Input.GetMouseButtonUp(0))
            {
                space = false;
                am.SetBool("ThrowHook", true);
            }     
        }

        if (changeColor)
        {
            ChangeColorOfRod();        
        }
    }

    void FixedUpdate()
    {
        RodFaceToHook();
        rodBend();         
    }

    public void shootHook()
    {
        rope.GetComponent<Rope>().ena = true;
        rb.isKinematic = false;
        rb.constraints = RigidbodyConstraints.None;
        rb.useGravity = true;

        hook.transform.parent = null;
        rb.AddForce((rb.transform.forward + (Vector3.up * 0.15f)) * power);

        timeAfterWeGetFish = Random.Range(2f, 6f);

        progressBar.transform.parent.gameObject.SetActive(false);

        StartCoroutine(weGetFish());
    }

    private void throwHook()
    {
        am.SetBool("LoadHook", true);

        power += speedPower * Time.deltaTime;

        if (power >= maxPower)
        {
            power = maxPower;
        }

        progressBar.GetComponent<Image>().fillAmount = power / maxPower;
    }

    private void ChangeColorOfRod()
    {
        float angle = hookMovement.angleBeetweenHookAndRod;
        float scale = hookMovement.scaleForAngle;
        float red = angle / scale;

        if (float.IsNaN(red))
        {
            red = 0;
        }

        if (red > 1)
        {
            red = 1;
        }

        colorOfRod = new Color(1f, 1f - red, 1f - red);

        Material m = new Material(s);
        m = rodRenderer.material;

        m.color = colorOfRod;

        rodRenderer.material = m;
    }

    private IEnumerator weGetFish()
    {
        yield return new WaitForSeconds(timeAfterWeGetFish);

        float distanceBeetweenPlayerAndHook = Vector3.Distance(transform.position, hook.transform.position);

        if(distanceBeetweenPlayerAndHook < 10f)
        {
            hook.GetComponent<Hook>().restartHook();
            yield break;
        }

        bend = true;
        changeColor = true;

        hook.transform.GetChild(1).GetComponent<Animator>().SetBool("upAndDown", true);

        whatFish = Random.Range(0, 3);

        if (distanceBeetweenPlayerAndHook < 25)
        {
            whatFish = 0;
        }

        switch (whatFish)
        {
            case 1:
                createFish(Fish, offsetBeetweenHookAndFish, Quaternion.Euler(0f, 0f, -40f));
                break;
            case 2:
                createFish(BigFish, new Vector3(-0.227f, 1.253f, -0.094f), Quaternion.Euler(-90f, 0f, 0f));
                break;
            case 0:
                createFish(littleFish, new Vector3(-0.187f, 1.982f, -0.122f), Quaternion.Euler(0f, 90f, -180f));
                break;
        }

        hook.GetComponent<Hook>().enabled = true;
        StartCoroutine(hook.GetComponent<Hook>().fishGoInRandonDir());
    }

    public void createFish(GameObject f, Vector3 offset, Quaternion q)
    {
        GameObject p = Instantiate(f, hook.transform.position + offset, q);
        p.transform.parent = hook.transform;
        hook.GetComponent<Hook>().fish = p;
    }


    public void restart()
    {
        rope.GetComponent<Rope>().ena = false;
        rope.GetComponent<LineRenderer>().startColor = Color.black;
        rope.GetComponent<LineRenderer>().endColor = Color.black;

        hook.GetComponent<Hook>().timer = 0f;
        hook.GetComponent<Hook>().timeLeft = 0f;

        progressBarPowerOfFish.GetComponent<Image>().fillAmount = 0f;

        hook.transform.rotation = new Quaternion(0f, 0f, 0f, 0);

        am.SetBool("ThrowHook", false);
        am.SetBool("LoadHook", false);
        am.SetBool("goUpFish", false);
        am.SetBool("getFish", false);

        floatForFish.GetComponent<Animator>().SetBool("upAndDown", false);

        bend = false;
        space = true;
        power = 0f;
        hook.GetComponent<Hook>().HPOfRope = 100f;

        progressBar.GetComponent<Image>().fillAmount = 0f;
        progressBar.transform.parent.gameObject.SetActive(true);

        hook.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        hook.GetComponent<Rigidbody>().isKinematic = true;

        StartCoroutine(setOffRot());

        changeColor = false;

        rodRenderer.material.color = Color.white;
    }

    public void RodFaceToHook()
    {
        Vector3 lookPos = hookFloat.transform.position - fishingRodWithBones.transform.position;
        float damping = 5f;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        fishingRodWithBones.transform.rotation = Quaternion.Slerp(fishingRodWithBones.transform.rotation, rotation, Time.deltaTime * damping);
    }

    private void rodBend()
    {
        float v = hook.GetComponent<Hook>().velocity;

        if(bend)
        {
            Vector3 toGo = new Vector3(0f, 0f, (v / 10f) * 50);
            Vector3 myVec = Vector3.Lerp(Rod.transform.localEulerAngles, toGo, Time.deltaTime);
            Rod.transform.localEulerAngles = myVec;
        }
        else
        {
            Vector3 toGo = new Vector3(0f, 0f, 0f);
            Vector3 myVec = Vector3.Lerp(Rod.transform.localEulerAngles, toGo, Time.deltaTime);
            Rod.transform.localEulerAngles = myVec;
        }     
    }

    IEnumerator setOffRot()
    {
        yield return new WaitForSeconds(1f);
        transform.GetComponentInParent<PlayerRot>().goRot = true;
        am.SetBool("fail", false);
    }

    public void goToLeft()
    {
        fishingRodWithBones.transform.localRotation = Quaternion.Slerp(fishingRodWithBones.transform.localRotation, Quaternion.Euler(0, 0, 50f), 2f * Time.deltaTime);
    }

    public void goToRight()
    {
        fishingRodWithBones.transform.localRotation = Quaternion.Slerp(fishingRodWithBones.transform.localRotation, Quaternion.Euler(0, 0, -50f), 2f * Time.deltaTime);
    }

    public void desFishAndAddPoints()
    {    
        int howMuchKG = whatFish;

        pointsNumber += 5 * (howMuchKG + 1);
        points.text = pointsNumber.ToString() + "kg";

        if(hook.GetComponent<Hook>().fish != null)
        {
            Destroy(hook.GetComponent<Hook>().fish.gameObject);
        }
        
        restart();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hook : MonoBehaviour
{
    public GameObject player;
    public GameObject rope;
    public GameObject fishingRod;
    public GameObject imageFill;
    public GameObject rod;
    public GameObject fish;

    public Transform posOfRod;
    public LineRenderer line;
    public FishingRod fr;

    private Rigidbody rb;

    public float HPOfRope = 100f;
    public bool go = false;
    public float velocity;
    public float forceMode = 0f;
    public float timeLeft;
    public float timer = 0f;
    public float angleBeetweenHookAndRod;
    public float scaleForAngle;

    private bool changeDir = false;
    private float maxDistance = 90f;
    private Vector3 randomDir;
    private float velUp;
    private float veldDown;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        timeLeft = timer;
    }

    private void FixedUpdate()
    {
        if (go)
        {
            timeLeft -= Time.deltaTime;

            if (timeLeft < 0)
            {
                fishGoInRandomDir();
            }

            if (Input.GetMouseButton(1))
            {
                pullFish();
            }
            else
            {
                healTheRope();
            }

            lineChangeColor();
            checkGameState();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other)
        {
            rb.constraints = RigidbodyConstraints.FreezePositionY;
            rb.velocity = new Vector3(0f, 0f, 0f);
        }
    }

    protected void LateUpdate()
    {
        transform.localEulerAngles = new Vector3(0, 0, transform.localEulerAngles.z);
    }

    public IEnumerator fishGoInRandonDir()
    {
        yield return new WaitForSeconds(1f);

        go = true;

        rb.constraints &= ~RigidbodyConstraints.FreezePositionX;
        rb.constraints &= ~RigidbodyConstraints.FreezePositionZ;
    }

    private void pullFish()
    {
        rope.GetComponent<Rope>().upCurve();

        Vector3 dirToPlayer = (player.transform.position - transform.position).normalized;

        if (imageFill.GetComponent<Image>().fillAmount < 0.61f)
        {
            rb.velocity = dirToPlayer * (12f - velocity);
            changeDirection();
        }
        else
        {
            rb.velocity = randomDir * (velocity - 5f);
            changeDirection();
            HPOfRope -= Time.deltaTime * 20;
        }
    }

    private void changeDirection()
    {
        float powerOfPlayer = 3f;

        float dNorm = Vector3.Distance(transform.position, player.transform.position) / maxDistance;

        if (Input.GetKey("a"))
        {
            rb.velocity -= (transform.right * powerOfPlayer * (1f - dNorm));

            fr.goToLeft();
        }
        else if (Input.GetKey("d"))
        {
            rb.velocity += (transform.right * powerOfPlayer * (1f - dNorm));
            fr.goToRight();
        }

    }

    public void lineChangeColor()
    {
        Color r1 = new Color((100f - HPOfRope) / 100f, 0f, 0f);
        Color r2 = new Color((100f - HPOfRope) / 100f, 0f, 0f);

        line.startColor = r1;
        line.endColor = r2;
    }

    private void healTheRope()
    {
        rope.GetComponent<Rope>().downCurve();

        rb.velocity = randomDir * velocity;
        HPOfRope += Time.deltaTime * 10;
    }

    private void fishGoInRandomDir()
    {
        veldDown = fish.GetComponent<Fish>().velDown;
        velUp = fish.GetComponent<Fish>().vedUp;

        Debug.Log(veldDown + " " + velUp);

        Vector3 dirToPlayer = (player.transform.position - transform.position).normalized;

        float posX = dirToPlayer.x;
        float posZ = dirToPlayer.z;

        float XRange = 0.5f;
        float ZRange = 2f;

        if (posX > 0)
        {
            posX = -(Random.Range(0f, XRange));
        }
        else
        {
            posX = Random.Range(0f, XRange);
        }

        if (posZ > 0)
        {
            posZ = -(Random.Range(1f, ZRange));
        }
        else
        {
            posZ = Random.Range(1f, ZRange);
        }

        randomDir = new Vector3(posX, 0f, posZ);

        velocity = Random.Range(veldDown, velUp);

        imageFill.GetComponent<Image>().fillAmount = velocity / 10f;

        rb.velocity = randomDir * velocity;

        timer = Random.Range(1f, 4f);
        timeLeft = timer;
    }

    public void checkGameState()
    {
        Vector3 posOfHookInZ = new Vector3(transform.position.x, 0, transform.position.z);
        Vector3 posOfRodInZ = new Vector3(posOfRod.position.x, 0f, posOfRod.position.z);

        angleBeetweenHookAndRod = Vector3.Angle(posOfRodInZ, posOfHookInZ);
        scaleForAngle = 1500f / Vector3.Distance(transform.position, player.transform.position);

        if (HPOfRope <= 0)
        {
            restartHook();
            Debug.Log("Dead Rope");
        }
        else if (Vector3.Distance(transform.position, player.transform.position) > maxDistance)
        {
            restartHook();
        }
        else if (Vector3.Angle(posOfRodInZ, posOfHookInZ) > 1500f / Vector3.Distance(transform.position, player.transform.position))
        {
            restartHook();
        }
        else if (Vector3.Distance(transform.position, player.transform.position) < 7f)
        {

            rb.velocity = new Vector3(0f, 0f, 0f);
            transform.parent = player.transform;
            transform.localPosition = new Vector3(0.53f, -3f, 1.56f);
            rb.constraints = RigidbodyConstraints.FreezeAll;
            rb.useGravity = false;
            fishingRod.GetComponent<Animator>().SetBool("getFish", true);
            transform.GetChild(1).GetComponent<Animator>().SetBool("upAndDown", false);

            player.GetComponent<PlayerRot>().goRot = false;

            enabled = false;
        }
    }

    public void restartHook()
    {
        fishingRod.GetComponent<Animator>().SetBool("fail", true);
        fishingRod.GetComponent<FishingRod>().restart();
        rb.velocity = new Vector3(0f, 0f, 0f);
        transform.parent = player.transform;
        transform.localPosition = new Vector3(0.53f, -3f, 1.56f);
        rb.constraints = RigidbodyConstraints.FreezeAll;
        rb.useGravity = false;

        if (fish != null)
        {
            Destroy(fish.gameObject);
        }

        go = false;
    }
}

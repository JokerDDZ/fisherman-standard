﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rope : MonoBehaviour
{
    public GameObject fish;
    public GameObject endPointOfTheRod;
    public AnimationCurve curve;

    private GameObject begin;
    private GameObject end;
    
    LineRenderer lr;
    
    public float blend = 2f;
    public bool ena = false;

    private int children;

    // Start is called before the first frame update
    void Start()
    {
        children = transform.childCount;

        begin = transform.GetChild(0).gameObject;
        end = transform.GetChild(children - 1).gameObject;

        children = transform.childCount;

        lr = GetComponent<LineRenderer>();
        lr.startWidth = 0.05f;
        lr.endWidth = 0.05f;
        lr.startColor = Color.black;
        lr.endColor = Color.black;
        lr.positionCount = children;
    }

    void Update()
    {
        for (int i = 0; i < children; i++)
        {
            lr.SetPosition(i, transform.GetChild(i).transform.position);
        }
    }


    void FixedUpdate()
    {
        if (ena)
        {
            begin.transform.position = endPointOfTheRod.transform.position;
            end.transform.position = fish.transform.position;

            Vector3 vec = end.transform.position - begin.transform.position;

            Vector3 distForPoints = vec / (transform.childCount - 1);

            for (int i = 1; i < transform.childCount - 1; i++)
            {
                Vector3 myXZ = begin.transform.position + (i * distForPoints);

                float x = Vector3.Distance(fish.transform.position, endPointOfTheRod.transform.position);

                float dis = Vector3.Distance(transform.GetChild(i).transform.position, fish.transform.position);
                float myX = dis / x;

                float y = curve.Evaluate(myX);

                transform.GetChild(i).transform.position = myXZ + (Vector3.down * blend * y);
            }

        }
        else
        {
            begin.transform.position = endPointOfTheRod.transform.position;
            end.transform.position = fish.transform.position;

            Vector3 vec = end.transform.position - begin.transform.position;

            Vector3 distForPoints = vec / (transform.childCount - 1);

            for (int i = 1; i < transform.childCount - 1; i++)
            {
                Vector3 myXZ = begin.transform.position + (i * distForPoints);

                float x = Vector3.Distance(fish.transform.position, endPointOfTheRod.transform.position);

                float dis = Vector3.Distance(transform.GetChild(i).transform.position, fish.transform.position);
                float myX = dis / x;

                transform.GetChild(i).transform.position = myXZ;
            }
        }
    }

    public void downCurve()
    {
        blend += Time.deltaTime;

        if (blend > 1)
        {
            blend = 1;
        }
    }

    public void upCurve()
    {
        blend -= Time.deltaTime;

        if (blend < 0)
        {
            blend = 0;
        }
    }
}
